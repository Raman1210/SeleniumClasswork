package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;


public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
	
	PageFactory.initElements(driver, this);
	}

	public EditLeadPage clickFindLead() {
		WebElement eleFindLead= locateElement("linkText", "Find Leads");
		click(eleFindLead);
		return this; 
	}

	public EditLeadPage clickPhoneTab() {
		WebElement elePhoneTab= locateElement("xpath", "//span[text()='Phone']");
		click(elePhoneTab);
		return this; 
	}

	public EditLeadPage typePhoneNumber(String data) {
		WebElement elePhoneNumber = locateElement("name", "phoneNumber");
		type(elePhoneNumber, data);
		return this;
	}


	public EditLeadPage clickFindLeads() {
		WebElement eleFindLeads= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		return this; 
	}
	@CacheLookup
	@FindBy(xpath= "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleFirstResult;
	
	public EditLeadPage clickFirstResult() {
		//WebElement eleFirstResult= locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFirstResult);
		return this; 
	}


	public EditLeadPage clickEditButton() {
		WebElement eleEditButton= locateElement("linktext", "Edit");
		click(eleEditButton);
		return this; 
	}

	public EditLeadPage editCompanyName(String data) {
		WebElement eleCompanyName= locateElement("id", "updateLeadForm_companyName");
		type(eleCompanyName, data);
		return this; 
	}


	public EditLeadPage clickSubmit() {
		WebElement eleSubmit= locateElement("class", "smallSubmit");
		click(eleSubmit);
		return this; 
	}

}

