package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.EditLeadPage;
import pages.MyHomePage;
import wdMethods.ProjectMethods;

import org.testng.annotations.BeforeClass;

public class TC002_EditLead extends ProjectMethods{
	
	@BeforeClass
	public void Data() {
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC002";
	}
	
	@Test(dataProvider="fetchData")
	public  void EditLead(String phnNumber, String updateCname)   {
		new EditLeadPage()
		.clickFindLead()
		.clickPhoneTab()
		.typePhoneNumber(phnNumber)
		.clickFindLeads()
		.clickFirstResult()
		.clickEditButton()
		.editCompanyName(updateCname);
		
		
	}

}
