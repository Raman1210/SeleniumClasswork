package Week5.Day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Testcase1 extends CommonMethods  {
	@Test
	public void facebook() {
		startApp("chrome", "https://www.facebook.com/");
		WebElement eleemail = locateElement("xpath", "//table[@role=\"presentation\"]//following::input");
		type(eleemail, "aishu_sweet@ymail.com");
		WebElement elepassword = locateElement("xpath","//table[@role=\"presentation\"]//following::input//following::input");
		type(elepassword, "SA_15_sa");
		WebElement elelogin = locateElement("id","loginbutton");
		click(elelogin);
		WebElement eleSearch = locateElement("xpath","//input[@class='_1frb']");
		type(eleSearch,"Testleaf");
		WebElement eleSearchicon = locateElement("xpath","//i[@class=\"_585_\"]");
		click(eleSearchicon);

		WebElement eleTestleaf = locateElement("xpath","//div[text()= 'TestLeaf']");
		String text = eleTestleaf.getText();
		if(text.equals("TestLeaf")) {
			System.out.println("Page"+text+"Launched successfully");
		}
		click(eleTestleaf);
		String newTitle = driver.getTitle();
		System.out.println(newTitle);
		WebElement elelikeno = locateElement("xpath","//div[contains(@class,\"clearfix _ikh\")]//following::div//following::div//following::div//div");
		String text1 = elelikeno.getText();
		System.out.println("Totally -"+ text1);
		WebElement elelogout = locateElement("xpath","//div[@id='userNavigationLabel']");
		click(elelogout);
		WebElement elelogout1 = locateElement("xpath","//div[@id='userNavigationLabel']");
		click(elelogout1);
	
	}
}


