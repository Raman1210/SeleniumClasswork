package Week5.Day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.remote.RemoteWebDriver;


public class CommonMethods implements Methodsignature {
	public int i = 1;
	public RemoteWebDriver driver;
	ChromeOptions op;

	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				op=new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (Exception e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}

	private void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snapshot/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}



	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 	: return driver.findElementById(locValue);
			case "class" 	: return driver.findElementByClassName(locValue);
			case "xpath" 	: return driver.findElementByXPath(locValue);
			case "linkText"	: return driver.findElementByLinkText(locValue);
			case "partiallinktest": return driver.findElementByPartialLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");

		}

		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}



	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
		i++;
	}

	@Override
	public String getText(WebElement ele) {

		WebElement findElement = driver.findElement(By.xpath(""));
		String text = findElement.getText();
		System.out.println(text);
		return text;

	}

	@Override
	public void acceptAlert() {

	}

	@Override
	public void closeBrowser() {
		driver.close();


	}

	@Override
	public void switchToWindow(int index) {
	}	
		
	}



