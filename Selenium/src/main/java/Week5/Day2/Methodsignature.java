package Week5.Day2;

import org.openqa.selenium.WebElement;

public interface Methodsignature {

	//To launch URL
	public void startApp(String browser,String url);

	//To locate elements
	public WebElement locateElement(String locator, String locValue) ;	

	// To locate elements using Identifiers
	public WebElement locateElement(String locValue) ;	
	
	//To enter values in fields
	
	public void type(WebElement ele, String data) ;

	//To click on elements
	public void click(WebElement ele);
	
	// To get a string value
	
	public String getText(WebElement ele);

	// To take snapshot
	
	public void acceptAlert();
	
	// To handle windows
	public void switchToWindow(int index);


	// To close browser
	public void closeBrowser();

}
