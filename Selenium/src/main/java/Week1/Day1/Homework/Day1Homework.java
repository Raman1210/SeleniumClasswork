package Week1.Day1.Homework;

public class Day1Homework {
	//Using all Arithmatic operators

	static void arithmaticoperators() {
		int a=10;
		int b=20;
		int c=30;
		System.out.println("Sum:"+(a+b+c));
		System.out.println("Subtract:"+(b-a));
		System.out.println("Multiply:"+(b*c));
		System.out.println("Division:"+(b/a));
		System.out.println("Mod:"+(c%a));

	}

	//Using all Relational operators

	static void relationaloperators() {
		int a=10;
		int b=20;
		int c=10;
		System.out.println("Greaterthan:"+(b>a));
		System.out.println("Greaterthan equal to:"+(b>=a));
		System.out.println("Less Than equal to:"+(a<=b));
		System.out.println("Less Than:"+(a<b));
		System.out.println("Equal to:"+(a==c));
		System.out.println("Not Equal to:"+(a!=b));
	}

	//Create 9 primitive Datatypes

	public static void main(String[] args) {
		String Name = "Aishwarya";
		long MobileNo = 8939299647l;
		float interest = 10.5f;
		double TolInterest = 33.5d;
		int SNo= 123;
		short count= 26;
		char gender='F';
		byte countof=15;//-128 to +127
		boolean newtojava=true;

		System.out.println("Name:"+Name);
		System.out.println("Mobile Number:"+MobileNo);
		System.out.println("Interest:"+TolInterest);
		System.out.println("Serial Number:"+SNo);
		System.out.println("Count:"+count);
		System.out.println("Gender:"+gender);
		System.out.println("Count of:"+countof);
		System.out.println("Newtojava:"+newtojava);
		System.out.println("--------------------");
		arithmaticoperators();
		System.out.println("--------------------");
		relationaloperators();

	}

}
