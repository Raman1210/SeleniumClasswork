package Week3.Day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {
	public static void main(String[] args) {
		//Set Driver
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//To invoke browser and launch URL
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		// Enter user name and Password to login
		driver.findElementById("username").sendKeys("Demosalesmanager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		// click hyperlink to navigate
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		//Create a lead
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("Aishwarya");
		driver.findElementById("createLeadForm_lastName").sendKeys("S");
		//driver.findElementByClassName("smallSubmit").click();
		//Drop Downs by Visible Text
		WebElement dropdown = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(dropdown);
		dd.selectByVisibleText("Conference");
		//Drop Down by Value
		WebElement dropdown1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(dropdown1);
		dd1.selectByValue("CATRQ_CARNDRIVER");
		//Print all Options from the drop down
		
		List<WebElement> alloptions = dd.getOptions();
		for(WebElement eachoptions:alloptions)
		{
		System.out.println(eachoptions.getText());
		}
		driver.findElementByClassName("smallSubmit").click();
	}

}

