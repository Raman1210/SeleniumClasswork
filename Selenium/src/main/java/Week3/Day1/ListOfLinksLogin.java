package Week3.Day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ListOfLinksLogin {
	public static void main(String[] args) {
		//Set Driver
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//To invoke browser and launch URL
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		//To get no of links in login page
		List<WebElement> byTagName = driver.findElementsByTagName("a");
		System.out.println(byTagName.size());
		//To click on second link in login page
		byTagName.get(1).click();
	}

}
