package Week3.Day2;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class findleadsWait {

	public static void main(String[] args) throws StaleElementReferenceException {

		//Set Driver
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//To invoke browser and launch URL
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		//Maximize
		driver.manage().window().maximize();
		// Enter user name and Password to login
		driver.findElementById("username").sendKeys("Demosalesmanager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		// click hyperlink to navigate
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//div[@style='padding-left:155px']/following::input").sendKeys("Aish");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait = new WebDriverWait(driver,2);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@style='overflow: visible;']//div//div//div//a"))).click();

	}

}
