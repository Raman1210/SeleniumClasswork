package Week3.Day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class erailTable {
	public static void main(String[] args) throws IndexOutOfBoundsException {
		//Set Driver
				System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
				//To invoke browser and launch URL
				ChromeDriver driver = new ChromeDriver();
				driver.get("https://erail.in/trains-between-stations/chennai-central-MAS/bangalore-east-BNCE?view=d");
				//Maximize
				driver.manage().window().maximize();
				boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
				if(selected) {
					driver.findElementById("chkSelectDateOnly").click();
				}
				WebElement table = driver.findElementByXPath("//td[@id='tdMainDiv']");
				
				List<WebElement> tr = table.findElements(By.tagName("tr"));
				//System.out.println(tr.size());
				//WebElement firstrow = tr.get(0);
				
				for (WebElement eachTr : tr) {
					
					String text = eachTr.findElements(By.tagName("td")).get(1).getText();
					System.out.println(text);
				}
	}

}
