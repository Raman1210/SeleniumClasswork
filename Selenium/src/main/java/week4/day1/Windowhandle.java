package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandle {
	public static void main(String[] args) {
		//Set Driver
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//To invoke browser and launch URL
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");

		//Maximize
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allwindows=driver.getWindowHandles();
		List<String> listOfWindows = new ArrayList<String>();
		listOfWindows.addAll(allwindows);
		System.out.println(allwindows.size());

		String secondWindow = listOfWindows.get(1);
		driver.switchTo().window(secondWindow);
		String newTitle = driver.getTitle();
		System.out.println(newTitle);
		String currentURL = driver.getCurrentUrl();
		System.out.println(currentURL);
		driver.quit();

	}

}
