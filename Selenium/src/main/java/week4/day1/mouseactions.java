package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class mouseactions {
	
	public static void main(String[] args) {
		//Set Driver
				System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
				//To invoke browser and launch URL
				ChromeDriver driver = new ChromeDriver();
				driver.get("https://jqueryui.com/draggable/");

				//Maximize
				driver.manage().window().maximize();
				
				driver.switchTo().frame(0);
				WebElement drag = driver.findElementByXPath("//div[@id='draggable']");
				Actions builder = new Actions(driver);
				builder.dragAndDropBy(drag, 100, 100).perform();
				driver.switchTo().defaultContent();
				driver.findElementByLinkText("Droppable").click();
				driver.switchTo().frame(0);
				WebElement drag1 = driver.findElementByXPath("//div[@id='draggable']");
				WebElement drop = driver.findElementByXPath("//div[@id='droppable']");
				Actions builder1 = new Actions(driver);
				builder1.dragAndDrop(drag1,drop).perform();
				
	}

}
