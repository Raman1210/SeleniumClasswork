package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertsFrames {
	
public static void main(String[] args) {
	//Set Driver
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			//To invoke browser and launch URL
			ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
			//Maximize
			driver.manage().window().maximize();
			driver.switchTo().frame("iframeResult");
			driver.findElementByXPath("//button[text()= 'Try it']").click();
			driver.switchTo().alert().sendKeys("XYZ");
			driver.switchTo().alert().accept();
			System.out.println(driver.findElementById("demo").getText());
			driver.switchTo().defaultContent();				
			}
}



