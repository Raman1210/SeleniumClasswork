package methods;

import org.openqa.selenium.WebElement;
//import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;


public class TC004_DeleteLead extends PSM {
	@Test(dependsOnMethods = "methods.TC003_EditLead.editlead")

	public void deleteLead() throws InterruptedException {
		login();			
		//WebElement crmlink = locateElement("linkText", "CRM/SFA");
		//click(crmlink);
		WebElement eleCreateLead = locateElement("linkText","Create Lead");
		click(eleCreateLead);
		WebElement leadmenu = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(leadmenu);
		WebElement findlead = locateElement("linkText", "Find Leads");
		click(findlead);
		WebElement tapPhnTab = locateElement("xpath", "(//a[@class = 'x-tab-right'])[2]");
		click(tapPhnTab);
		//WebElement PhnAreaCode = locateElement("xpath", "//input[@name = 'phoneAreaCode']");
		//type(PhnAreaCode, "01");
		WebElement PhnNmbr = locateElement("xpath", "//input[@name = 'phoneNumber']");
		type(PhnNmbr, "9566234725");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		String getID = pickalead.getText();
		click(pickalead);		
		
		WebElement deletelink = locateElement("linkText", "Delete");
		click(deletelink);
		WebElement findDeletedLead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findDeletedLead);
		WebElement LeadID = locateElement("xpath", "//input[@name = 'id']");
		type(LeadID, getID);
		WebElement filtrleadagain = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrleadagain);
		closeBrowser();
		
		
		
		
	}


}
