package methods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import methods.SeMethods;

public class TC006_DuplicateLead extends SeMethods{

	@Test
	public void duplicatelead() throws InterruptedException {
		login();
		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
		WebElement leadmenu = locateElement("LinkText", "Leads");
		click(leadmenu);
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findlead);
		WebElement tapemailTab = locateElement("xpath", "(//a[@class = 'x-tab-right'])[3]");
		click(tapemailTab);
		WebElement enteremail = locateElement("xpath", "//input[@name = 'emailAddress']");
		type(enteremail, "xyz@gmail.com");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		WebElement capturename= locateElement("xpath", "(//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-firstName '])[1]");
		String getID = capturename.getText();
		click(pickalead);
		WebElement duplicatelead = locateElement("LinkText", "Duplicate Lead");
		click(duplicatelead);
		verifyTitle("Duplicate Lead");
		WebElement createlead = locateElement("class", "smallSubmit");
		click(createlead);
		WebElement duplead = locateElement("id", "viewLead_firstName_sp");
		String actual = duplead.getText();

		if(actual.contains(getID))
		{
			System.out.println("Success");
		}else
			System.out.println("Failure");
		closeBrowser();
	}

	private void login() {
		// TODO Auto-generated method stub
		
	}

}
