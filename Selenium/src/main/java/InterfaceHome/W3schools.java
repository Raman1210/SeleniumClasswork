package InterfaceHome;

import org.openqa.selenium.WebElement;

public interface W3schools {
	
	//Launch Browser and URL
	
	public void invokeapp(String browser,String URL);
	
	// Login Functionality
	
	public WebElement identify(String Locator, String Locvalue);
	
	//To enter values in fields
	
	public void EnterText(WebElement identifier, String value);
	
	//To press the functionality
	
	public void click (WebElement press);
	
	
	//To take Snapshot
	//Created by Aishu
	
	public void snapshot();
	
}
